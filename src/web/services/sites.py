import requests

from web.models import Site, SiteHistory
from web.services.notifications import UsersNotificationMap
from web.tasks import send_notifications_task


def check_sites(ids=None):
    sites = Site.objects.all()

    if ids is not None:
        sites = sites.filter(id__in=ids)

    history_objects = []
    site_objects = []

    users_notification_map: UsersNotificationMap = {}

    for site in sites:
        status_code = None

        previous_status = site.status

        try:
            response = requests.get(site.url, timeout=10)
            status_code = response.status_code
            error_response_content = response.text if not response.ok else None
        except requests.exceptions.ConnectionError as exc:
            error_response_content = str(exc)

        history = SiteHistory(site=site, status_code=status_code, error_response_content=error_response_content)

        site.status = history.is_success

        if previous_status != site.status:
            if site.user_id not in users_notification_map:
                users_notification_map[site.user_id] = []
            users_notification_map[site.user_id].append(site.id)

        site_objects.append(site)
        history_objects.append(history)

    SiteHistory.objects.bulk_create(history_objects)
    Site.objects.bulk_update(site_objects, ["status"])
    send_notifications_task.delay(users_notification_map)

    return history_objects
