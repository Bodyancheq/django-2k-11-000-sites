from rest_framework.permissions import BasePermission


class BlockListPermission(BasePermission):
    ips = ["10.17.0.229"]

    def has_permission(self, request, view):
        ip = request.META["REMOTE_ADDR"]
        return ip not in self.ips
