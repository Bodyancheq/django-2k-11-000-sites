function setSiteHistoryHtml(html) {
    const siteHistoryElement = document.querySelector("#site_history");
    siteHistoryElement.innerHTML = html;
}

function processHistoryResponse(data) {
    let html = '<ul>';

    // for history_item in data:
    for (const historyItem of data) {
        let statusHtml;
        if (historyItem.is_success) {
            statusHtml = '<span class="badge bg-success">OK</span>';
        }
        else {
            statusHtml = '<span class="badge bg-danger">ERROR</span>';
        }
        html += `<li>${statusHtml} ${historyItem.status_code || ''} - ${historyItem.created_at}</li>`;
    }

    html += '</ul>';
    setSiteHistoryHtml(html);
}

function downloadSiteHistory() {
    setSiteHistoryHtml("Идет загрузка данных...");
    fetch(`/api/sites/${siteId}/history/`)
        .then(function (response) {
            return response.json()
        })
        .then(function (data) {
            processHistoryResponse(data);
        });
}

document.addEventListener("DOMContentLoaded", function() {
    downloadSiteHistory();

    const buttonElement = document.querySelector("#site_history_update_button");
    buttonElement.addEventListener("click", function() {
        downloadSiteHistory();
    })
});
