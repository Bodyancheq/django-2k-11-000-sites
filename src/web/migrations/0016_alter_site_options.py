# Generated by Django 3.2.7 on 2021-12-10 13:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("web", "0015_auto_20211210_1346"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="site",
            options={
                "permissions": [("check", "Проверка сайта")],
                "verbose_name": "сайт",
                "verbose_name_plural": "сайты",
            },
        ),
    ]
