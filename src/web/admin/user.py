from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext, gettext_lazy as _


class UserModelAdmin(UserAdmin):
    ordering = ("-created_at",)
    list_display = ("id", "email", "is_superuser", "created_at")
    list_filter = ("is_superuser", "created_at")
    search_fields = ("id", "email")
    readonly_fields = ("telegram_user_id",)
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Permissions"),
            {
                "fields": ("role", "is_superuser", "groups", "user_permissions"),
            },
        ),
        (
            None,
            {
                "fields": (
                    "avatar",
                    "telegram_user_id",
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
