from django.contrib import admin

from web.admin.site import SiteModelAdmin
from web.admin.user import UserModelAdmin
from web.models import Site, User, SiteComment

admin.site.register(Site, SiteModelAdmin)
admin.site.register(User, UserModelAdmin)
admin.site.register(SiteComment)
