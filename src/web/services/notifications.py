import traceback
from smtplib import SMTPDataError
from typing import Dict, List

from django.template.loader import render_to_string

from testdjango.emails import send_html_email
from web.models import Site, User
from web.services.telegram import send_message

UsersNotificationMap = Dict[int, List[int]]
SiteUrl = str
SiteStatus = bool
SitesStatusDict = Dict[SiteUrl, SiteStatus]


def send_telegram_notification(user: User, sites_status_dict: SitesStatusDict):
    plain_text = render_to_string("web/email/site_status.txt", {"sites_status_dict": sites_status_dict})
    if user.telegram_user_id is not None:
        send_message(user, plain_text)


def send_email_notification(user: User, sites_status_dict: SitesStatusDict):
    context = {"sites_status_dict": sites_status_dict}
    plain_text = render_to_string("web/email/site_status.txt", context)
    html_message = render_to_string("web/email/site_status.html", context)
    try:
        send_html_email("Информация о доступности сайтов", plain_text, html_message, [user.email])
    except SMTPDataError:
        traceback.print_exc()


def send_notifications(users_notification_map: UsersNotificationMap):
    users = User.objects.filter(id__in=users_notification_map.keys())
    users_map: Dict[int, User] = {}
    for user in users:
        users_map[user.id] = user

    for user_id, site_ids in users_notification_map.items():
        if user_id not in users_map:
            continue

        sites_status_dict = {}
        for site in Site.objects.filter(id__in=site_ids):
            sites_status_dict[site.url] = site.status

        user = users_map[user_id]
        send_telegram_notification(user, sites_status_dict)
        send_email_notification(user, sites_status_dict)
