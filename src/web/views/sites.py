from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, DetailView, UpdateView, ListView

from web.forms import SiteForm
from web.models import Site, SiteHistory
from web.services.sites import check_sites


class SiteQuerysetMixin:
    def get_queryset(self):
        return (
            Site.objects.all()
            .select_related("user")
            .prefetch_related(Prefetch("history", SiteHistory.objects.order_by("-created_at")))
            .filter(user=self.request.user)
            .order_by("id")
        )


class SiteCreateView(LoginRequiredMixin, CreateView):
    template_name = "web/sites/edit.html"
    form_class = SiteForm
    success_url = reverse_lazy("sites")

    def form_valid(self, form):
        form.instance.user_id = self.request.user.id
        return super(SiteCreateView, self).form_valid(form)


class SiteDetailView(SiteQuerysetMixin, LoginRequiredMixin, DetailView):
    template_name = "web/sites/detail.html"

    def get_context_data(self, **kwargs):
        context = super(SiteDetailView, self).get_context_data()
        object: Site = context["object"]
        context["history"] = object.history.all().order_by("-created_at")
        return context


class SiteUpdateView(SiteQuerysetMixin, LoginRequiredMixin, UpdateView):
    template_name = "web/sites/edit.html"
    form_class = SiteForm

    def get_success_url(self):
        return reverse("site", args=(self.object.id,))


class SiteListView(SiteQuerysetMixin, LoginRequiredMixin, ListView):
    paginate_by = 10
    template_name = "web/sites/list.html"


@login_required
def site_check(request, pk: int):
    sites = Site.objects.all()
    site = get_object_or_404(sites, id=pk)
    check_sites([site.id])
    return redirect("site", pk=site.id)
