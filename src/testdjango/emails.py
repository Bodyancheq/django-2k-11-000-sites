from typing import List

from django.conf import settings
from django.core.mail import send_mail


def send_html_email(subject: str, plain_text: str, html_message, recipient_list: List[str]):
    send_mail(subject, plain_text, settings.DEFAULT_FROM_EMAIL, recipient_list, html_message=html_message)
