import time

from django.core.management import BaseCommand

from web.services.sites import check_sites


class Command(BaseCommand):
    help = "Check all sites"

    def add_arguments(self, parser):
        parser.add_argument("--daemon", dest="daemon", help="Start daemon process", type=bool)

    def handle(self, daemon, *args, **options):
        while True:
            site_history = check_sites()
            print(f"{len(site_history)} sites checked")
            if not daemon:
                break
            time.sleep(60)
