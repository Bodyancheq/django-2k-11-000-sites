from unittest.mock import patch

import pytest
import requests_mock

from web.services.sites import check_sites
from web.tests.factories import SiteFactory


@pytest.mark.parametrize("status_code, result", [(200, True), (400, False)])
@patch("web.tasks.send_notifications_task.delay")
def test_check_sites_notifications_about_failed_check(send_notifications_func, status_code, result):
    with requests_mock.Mocker() as m:
        m.get("http://site.local", status_code=status_code)

        SiteFactory(url="http://site.local")
        check_sites()
        if result:
            send_notifications_func.assert_called_with({})
        else:
            send_notifications_func.assert_called()
