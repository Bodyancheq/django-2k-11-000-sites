from web.models import User, TelegramHash
from web.services.utils import generate_hash


def register_user(email: str, password: str, avatar) -> User:
    user = User(email=email, avatar=avatar)
    user.set_password(password)
    user.save()
    return user


def create_telegram_auth_hash(user: User) -> TelegramHash:
    telegram_hash, created = TelegramHash.objects.get_or_create(user=user, defaults=dict(hash=generate_hash(30)))
    return telegram_hash
