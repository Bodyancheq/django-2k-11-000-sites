"""testdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from web.views.auth import login_view, register_view, logout_view, telegram_auth_start, unienv_auth_callback
from web.views.main import main_view, html_view
from web.views.sites import SiteListView, SiteCreateView, SiteDetailView, SiteUpdateView, site_check

urlpatterns = [
    path("", main_view, name="main"),
    path("login/", login_view, name="login"),
    path("login/telegram/", telegram_auth_start, name="login-telegram"),
    path("login/unienv_auth_callback/", unienv_auth_callback, name="unienv-auth-callback"),
    path("register/", register_view, name="register"),
    path("logout/", logout_view, name="logout"),
    path("html/", html_view, name="html"),
    path("sites/", SiteListView.as_view(), name="sites"),
    path("sites/add/", SiteCreateView.as_view(), name="site-add"),
    path("sites/<int:pk>/", SiteDetailView.as_view(), name="site"),
    path("sites/<int:pk>/edit/", SiteUpdateView.as_view(), name="site-edit"),
    path("sites/<int:pk>/check/", site_check, name="site-check"),
]
